<?php
/**
 * Check unicity of a response after submit,
 * allow to delete or keep current to have only one answer finally.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019-2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL
 * @version 0.3.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class unicityAfterSubmit extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $name = 'unicityAfterSubmit';
    protected static $description = 'Check an unicity of response after submit, allow to update or leave old response (and delete current one).';

    public function init()
    {
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');

        $this->subscribe('afterSurveyComplete');
    }

    /**
    */
    public function beforeSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->event->get('survey');
        $surveyColumnsInformation = new \getQuestionInformation\helpers\surveyColumnsInformation($surveyId, App()->getLanguage());
        $aQuestionList = $surveyColumnsInformation->allQuestionListData();
        $this->event->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'questions'=>array(
                    'type'=>'select',
                    'label'=>$this->gT("Questions for unicity"),
                    'options'=>$aQuestionList['data'],
                    'htmlOptions'=>array(
                        'multiple' => true,
                        'placeholder' => gT("None"),
                        'unselectValue' => "",
                        'options'=>$aQuestionList['options'], // In dropdown, but not in select2
                    ),
                    'current' => $this->get('questions', 'Survey', $surveyId, []),
                    'help' => $this->gT('Unicity was checked after submit, allowing to update or not previous data. If answer is empty : it was not added in checking.'),
                ),
                'withtoken' => array(
                    'type'=>'select',
                    'label'=>$this->gT("Use token"),
                    'options'=>array(
                        'one' => gT('Yes'),
                        'group' => $this->gT('With group from responseListAndManage'),
                    ),
                    'htmlOptions'=>array(
                        'empty'=>gT("No"),
                    ),
                    'current' => $this->get('withtoken', 'Survey', $surveyId, 'group'),
                ),
                'replaceold' => array(
                    'type'=>'select',
                    'label'=>$this->gT("Replace previous data"),
                    'options'=>array(
                        'replace' => gT('Yes'),
                        'keep' => gT('No'),
                        //'disable' => gT('[WIP] Disable submit, reload to submit page'),
                    ),
                    'current' => $this->get('replaceold', 'Survey', $surveyId, 'keep'),
                    'help' => $this->gT('Choose if you keep old data or if you replace old data in case of unicity. The response id was updated if you choose replace'),
                ),
            )
        ));
    }

    /**
    */
    public function newSurveySettings()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        foreach ($oEvent->get('settings') as $name => $value) {
            $this->set($name, $value, 'Survey', $oEvent->get('survey'));
        }
    }

    /**
     */
    public function afterSurveyComplete()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $surveyId = $this->event->get('surveyId');
        $responseId = $this->event->get('responseId');
        $aquestions = $this->get('questions', 'Survey', $surveyId, []);
        if (empty($aquestions)) {
            return;
        }
        if(!is_array($aquestions)) {
            $this->log('Invalid questions setting: '.CVarDumper::dumpAsString($aquestions), 'warning');
            return;
        }
        if (empty($responseId)) {
            return;
        }
        $oResponses = \Response::model($surveyId)->findByPk($responseId);
        if (empty($oResponses)) {
            $this->log('An empty reponses after submit, this can not happen without another plugin.', 'error');
            return;
        }
        $aResponses = $oResponses->getAttributes();
        $aColumns = array();
        foreach ($aquestions as $columnCode) {
            if (!empty($aResponses[$columnCode])) {
                $aColumns[] = $columnCode;
            }
        }
        if (empty($aColumns)) {
            $this->log('Questions was set but not found in existing columns.', 'warning');
            return;
        }
        $oCriteria = new CDbCriteria();
        foreach ($aColumns as $column) {
            if (!empty($aResponses[$column])) {
                $oCriteria->compare(Yii::app()->db->quoteColumnName($column), $aResponses[$column]);
            }
        }
        $withtoken = $this->get('withtoken', 'Survey', $surveyId, 'group');
        if (!empty($aResponses['token']) && $withtoken) {
            if ($withtoken == 'group') {
                $oCriteria->addInCondition(
                    'token',
                    $this->getTokensList($surveyId, $aResponses['token'])
                );
            } else {
                $oCriteria->compare('token', $aResponses['token']);
            }
        }
        $oCriteria->compare('id', "<>{$responseId}");
        if (\Response::model($surveyId)->count($oCriteria)) {
            /* A reponse was find */
            $message = '';
            $state = 'info';
            switch ($this->get('replaceold', 'Survey', $surveyId, 'keep')) {
                case 'replace':
                    \Response::model($surveyId)->deleteAll($oCriteria);
                    $message = $this->gT("An existing reponse with this data exist. Previous response was updated with the new information");
                    $state = 'warning';
                    break;
                case 'keep':
                default:
                    \Response::model($surveyId)->deleteByPk($responseId);
                    $message = $this->gT("An existing reponse with this data exist. Your data can not be submitted.");
                    $state = 'danger';
                    break;
            }
            $this->getEvent()->getContent($this)
                ->addContent("<p class='alert alert-{$state}'>{$message}</p>");
        }
    }

    /**
     * Return the list of token related by TokenUsersListAndManageAPI or responseListAndManage
     * @param integer $surveyId
     * @param string $token
     * @return string[]
     */
    private function getTokensList($surveyId, $token)
    {
        $tokensList = array($token => $token);
        if (!$this->surveyHasToken($surveyId)) {
            return $tokensList;
        }
        if (version_compare(App()->getConfig('TokenUsersListAndManageAPI', 0), '0.14', ">=")) {
            return \TokenUsersListAndManagePlugin\Utilities::getTokensList($surveyId, $token, false);
        }
        if (!Yii::getPathOfAlias('responseListAndManage')) {
            return $tokensList;
        }
        if (!class_exists('\responseListAndManage\Utilities')) {
            return $tokensList;
        }
        return \responseListAndManage\Utilities::getTokensList($surveyId, $token, true);
    }
}
