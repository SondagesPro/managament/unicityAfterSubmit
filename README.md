# unicityAfterSubmit

LimeSurvey plugin to check unicity of a response after submit.

## Usage

After activation of the plugin, you find a new «Simple plugin settings». You can choose any number of questions for checking unicity.

Unicity control can be done using token group of responseListAndManage plugin, single token or no control on token.

## Home page, support & copyright
- HomePage <https://extensions.sondages.pro/>
- Copyright © 2019-2020 Denis Chenu <https://sondages.pro>
- Support the project : [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/)
- Issues <https://gitlab.com/SondagesPro>
- Professional support <https://extensions.sondages.pro/1>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>

